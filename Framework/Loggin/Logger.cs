using System;
using System.IO;

namespace Framework.Login
{
    public class  Logger
    {
        private readonly string _filepath;

        public Logger(string testName, string filePath)
        {
            _filepath = filePath;
            using (var log = File.CreateText(_filepath))
            {
                log.WriteLine($"Starting timestamp: {DateTime.Now.ToLocalTime()}");
                log.WriteLine($"Test: {testName}");
            }
        }

        public void Info(string message)
        {
            Writeline($"[INFO]: {message}");
        }

        public void Step(string message)
        {
            Writeline($"[STEP]: {message}");
        }

        public void Warning(string message)
        {
            Writeline($"[WARNING]: {message}");
        }
        public void Error(string message)
        {
            Writeline($"[ERROR]: {message}");
        }
        public void Fatal(string message)
        {
            Writeline($"[FATAL]: {message}");
        }

        private void Writeline(string text)
        {
            using(var log = File.AppendText(_filepath))
            {
                log.WriteLine(text);
            }
        }

        private void Write(string text)
        {
            using(var log = File.AppendText(_filepath))
            {
                log.Write(text);
            }
        }
    }
}