using System;

namespace Framework.Models
{
    public class HealSpiritCard : Card
    {
        public override string Name {get;set;} = "Heal Spirit";
        public override int Cost {get;set;} = 1;
        public override string Type {get;set;} = "Spell";
        public override string Arena {get;set;} = "Arena 10";
    }
}
