﻿using System;

namespace Framework.Models
{
    public class BabyDragonCard : Card
    {
        public override string Name {get;set;} = "Hunter";
        public override int Cost {get;set;} = 1;
        public override string Type {get;set;} = "Troop";
        public override string Arena {get;set;} = "Arena 1";
    }
}
