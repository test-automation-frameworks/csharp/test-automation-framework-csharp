using Framework.Models;

namespace Framework.Services
{
    public class InMemoryCardService : ICardService
    {
        public Card GetCardByName(string cardName)
        {
           switch(cardName)
           {
               case "Heal Spirit":
                 return new HealSpiritCard();

               case "Baby Dragon":
                return new BabyDragonCard();

                default:
                 throw new System.ArgumentException("The card is not available: "+ cardName);
           }
        }
    }
}