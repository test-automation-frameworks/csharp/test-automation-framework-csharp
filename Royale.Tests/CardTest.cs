using System.Collections.Generic;
using System.IO;
using Framework.Models;
using Framework.Selenium;
using Framework.Services;
using NUnit.Framework;
using Royale.Pages;
using System.Linq;
using Framework;

namespace Royale.Tests
{
    public class CardTest
    {
        static string[] cardNames = {"Heal Spirit", "Hunter"};
        static IList<Card> apiCards = new ApiCardService().GetAllCards().Take(10).ToList();
        
       
        [Test,Category("cards2")]
        [TestCaseSource("apiCards")]
        [Parallelizable(ParallelScope.Children)]
        public void IceSpiritIsOnCardsPage(Card card)
        {
            var cardOnPage = Page.Cards.GoTo().GetCardByName(card.Name);
            Assert.That(cardOnPage.Displayed);
        }

        [Test,Category("cards")]
        [TestCaseSource("apiCards")]
        [Parallelizable(ParallelScope.Children)]
        public void CardHeaderAreCorrectOnCardDetailsPage(Card card)
        {
          Page.Cards.GoTo().GetCardByName(card.Name).Click();  
         
          var cardOnPage = Page.CardsDetails.GetBaseCard();
          
          Assert.AreEqual(card.Name, cardOnPage.Name);
          Assert.AreEqual(card.Type, cardOnPage.Type);
          Assert.AreEqual(card.Arena, cardOnPage.Arena);
       }
    }
}