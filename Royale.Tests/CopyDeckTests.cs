using System;
using Framework;
using Framework.Selenium;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Royale.Pages;
using Tests.Base;

namespace Royale.Tests
{
    public class CopyDeckTests : TestBase
    {

        [Test,Category("copydeck")] 
        public void UserCanCopyTheDeck()
        {
            Page.DeckBuilder.Goto();
            Page.DeckBuilder.AddCardsManually();
            Page.DeckBuilder.CopySuggestedDeck();
            Page.CopyDeck.Yes();          
            Assert.That( Page.CopyDeck.Map.CopiedMessage.Displayed);
        }
        
        [Test,Category("copydeck")]
        public void UserOpensAppStore()
        {
            Page.DeckBuilder.Goto().AddCardsManually();
            Page.DeckBuilder.CopySuggestedDeck();
            Page.CopyDeck.No().OpenAppStore();
            Assert.That(Driver.Title, Is.EqualTo("Clash Royale on the App Store"));
        }

        [Test,Category("copydeck")]
        public void UserOpensGooglePlay()
        {
            Page.DeckBuilder.Goto().AddCardsManually();
            Page.DeckBuilder.CopySuggestedDeck();
            Page.CopyDeck.No().OpenGooglePlay();
            Assert.AreEqual("Clash Royale - Apps on Google Play", Driver.Title);
        }

    }
}