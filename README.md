# test-automation-framework-csharp

https://testautomationu.applitools.com/test-automation-framework-csharp/chapter1.html


## Set up env
* Install SDK of .NetCore 2.2 or greater
* Extensions like C# and PackSharp in Visual Studio Code

## Commands used
* dotnet build  => To build the whole project
* dotnet sln add project_name => To add the projects into the solution file
* dotnet test =>  To execute the test files
* dotnet clean => clean solution
* dotnet test --filter testcategory=cards => to filter the tests
* dotnet test --filter testcategory=cards -- NUnit.NumberOfTestWorkers=4 => Quantity of nodes to run Parallalizable tests
* dotnet restore => after remove packages 


## Notes
* Using the PackSharp set the command the following command to load .vscode => Bootstrap Selenium
* Using the PackSharp set the command the following command to load .vscode => Developer: Reload Window 
* PackSharp: Remove Packages 
* PackSharp: Add Project Reference


## Patterns
 1) Page object Model
 2) Page Map Pattern (Used in this proyect)

## Comments
1) ThreadStatic means each instance of the test is going to have its own driver
2) **What does Driver.Current represent?** The current, unique instance of _driver attached to a thread or process
3) WebDriverWait by default calls the ExpectedCondition every 500 milliseconds until it returns successfully.
4)$x('//a') => this function on Chrome browser only applies for xpath locators
5)document.querySelector('a.buttonY) => js code
6)Race condition => A race condition occurs when two or more threads are able to access shared data and they try to change it at the same time
7)Lock => A mechanism that forces threads or processes to wait in line and go one by one