using Framework;
using Framework.Selenium;
using OpenQA.Selenium;

namespace Royale.Pages
{
    public class DeckBuilderPage : PageBase
    {
        public readonly DeckBuilderPageMap Map;

        public DeckBuilderPage()
        {
            Map = new DeckBuilderPageMap();
        }
        public DeckBuilderPage Goto()
        {
            FW.Log.Step("Click Deck Builder Link");
            HeaderNav.Map.DeckBuilderLink.Click();
            return this;
        }

        public void AddCardsManually()
        {
            Driver.Wait.Until(drvr => Map.AddCardsManuallyLink.Displayed);
            FW.Log.Step("click Add Cards Manually linl");
            Map.AddCardsManuallyLink.Click();
            Driver.Wait.Until(drvr => Map.CopyDeckIcon.Displayed);
        }

        public void CopySuggestedDeck()
        {
            FW.Log.Step("click Copy Deck Icon");
            Map.CopyDeckIcon.Click();
        }
    }

    public class DeckBuilderPageMap
    {
        public Element AddCardsManuallyLink => Driver.FindElement(By.CssSelector("a[href='/deckbuilder']"),"AddCardsManuallyLink");
        public Element CopyDeckIcon => Driver.FindElement(By.XPath("//a[text()='add cards manually']"),"CopyDeckIcon");
    }
}