using Framework.Models;
using Framework.Selenium;
using OpenQA.Selenium;

namespace Royale.Pages 
{
    public class CardsDetailsPage : PageBase
    {
        public readonly CardsDetailsMap Map;
        public CardsDetailsPage()
        {
            Map = new CardsDetailsMap();
        }

        public (string category, string arena) GetCardCategory()
        {
            var categories = Map.CardCategory.Text.Split(',');
            
            return (categories[0].Trim(), categories[1].Trim());
        }

        public Card GetBaseCard()
        {
            var (category, arena) = GetCardCategory();
            return new Card {
                Name = GetName(Map.CardName.Text),
                Type = category ,
                Arena = arena
            };
        }

        public string GetName(string cardName)
        {
            if(cardName.Contains("Spell"))
                return "tid_card_type_spell";
            else if(cardName=="Troop")
                return "tid_card_type_character";
            return cardName;
        }
    }


    public class CardsDetailsMap
    {     
        public Element CardName => Driver.FindElement(By.CssSelector("div[class*='cardName']"), "CardName");
        public Element CardCategory => Driver.FindElement(By.CssSelector("div[class*='card__rarity']"),"CardCategory");
    }

}