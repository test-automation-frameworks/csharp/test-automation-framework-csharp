using System;
using Framework.Selenium;

namespace Royale.Pages
{
    public class Page
    {
        [ThreadStatic]
        public static CardsPage Cards;

        [ThreadStatic]
        public static CardsDetailsPage CardsDetails;

        [ThreadStatic]
        public static CopyDeckPage CopyDeck;

        [ThreadStatic]
        public static DeckBuilderPage DeckBuilder;
        
        public static void Init()
        {
            Cards        = new CardsPage();
            CardsDetails = new CardsDetailsPage();
            CopyDeck     = new CopyDeckPage();
            DeckBuilder  = new DeckBuilderPage();
        }
    }
}