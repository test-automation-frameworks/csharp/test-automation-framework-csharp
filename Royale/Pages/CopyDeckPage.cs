using Framework.Selenium;
using OpenQA.Selenium;

namespace Royale.Pages
{
    public class CopyDeckPage
    {
        public readonly CopyDeckMap Map;
        public CopyDeckPage()
        {
            Map = new CopyDeckMap();
        }

        public CopyDeckPage Yes()
        {
            Map.YesButton.Click();
            Driver.Wait.Until(drvr => Map.CopiedMessage.Displayed);
            return this;
        }

        public CopyDeckPage No()
        {
            Map.NoButton.Click();
            AcceptCookies();
            Driver.Wait.Until(drvr => Map.OthersStoresButton.Displayed);
            return this;
        }
        public void AcceptCookies()
        {
            Map.AcceptCookiesButton.Click();
            Driver.Wait.Until(drvr => !Map.AcceptCookiesButton.Displayed);
        }

        public void OpenAppStore()
        {
            Map.AppStoreButton.Click();
        }

        public void OpenGooglePlay()
        {
            Map.GooglePlayButton.Click();
        }

       
    }
    public class CopyDeckMap
    {
        public Element YesButton           => Driver.FindElement(By.Id("button-open"),"YesButton");
        public Element CopiedMessage       => Driver.FindElement(By.CssSelector(".notes.active"),"CopiedMessage");
        public Element NoButton            => Driver.FindElement(By.Id("not-installed"),"NoButton");
        public Element AppStoreButton      => Driver.FindElement(By.XPath("//a[text()='App Store']"),"AppStoreButton");
        public Element GooglePlayButton    => Driver.FindElement(By.XPath("//a[text()='Google Play']"),"GooglePlayButton");
        public Element AcceptCookiesButton => Driver.FindElement(By.CssSelector("a.cc-btn.cc-dismiss"),"AcceptCookiesButton");
        public Element OthersStoresButton  => Driver.FindElement(By.Id("other-stores"),"OthersStoresButton");
    }
}